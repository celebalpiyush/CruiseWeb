@isTest
private class TestLeadAllocationTriggerHandler {
    static testmethod void unitLeadTestMethod(){
        
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u1 = new User(Alias = 'standt', Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles',
                          UserName=uniqueUserName);
        insert u1;
        User u = new User(Alias = 'standt1', Email='standarduser1@testorg.com',ManagerId = u1.Id,
                          EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles',
                          UserName=uniqueUserName +'a');
        insert u;
            Lead leadData = new Lead(Company = 'Test Lead',
                      LastName = 'Lead Last Name'
                      );
    		insert leadData; 
            
            Lead_Allocation__c laData = new Lead_Allocation__c();
            laData.Lead__c = leadData.ID;
            laData.Agent__c = u.Id;
            laData.Lead_Ignore_Time__c = datetime.newInstance(2017, 10, 25, 10, 30, 0);
            insert laData;
            laData.Lead_Ignore_Time__c = datetime.newInstance(2017, 10, 25, 12, 35, 0);
            update laData;
                
    }
}