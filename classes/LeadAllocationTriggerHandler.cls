public class LeadAllocationTriggerHandler{
    
    public static void afterUpdate(List<Lead_Allocation__c> newList, map<id,Lead_Allocation__c> oldMap) {
        leadIgnoredStatus(newList,oldMap);
    }
    
    public static void leadIgnoredStatus(List<Lead_Allocation__c> ListNewUpdate, map<id,Lead_Allocation__c> mapOldRecord) {
        List<Lead_Allocation__c> newList = [Select Id, Agent__c, Agent__r.Email,Agent__r.name, Agent__r.ManagerId,Agent__r.Manager.Email, Lead__c, Lead__r.Name, Lead_Ignore_Time__c From Lead_Allocation__c Where Id In: ListNewUpdate]; 
        Map<Id,Messaging.SingleEmailMessage> mapOfLeadAllocationRecord = new Map<Id,Messaging.SingleEmailMessage>();
        List<Messaging.SingleEmailMessage> lstMsg = new List<Messaging.SingleEmailMessage>();
        List<String> emailIdsList= new List<String>();
        //String baseUrl = System.URL.getSalesforceBaseUrl().getHost();
        EmailTemplate templateId = [Select Id From EmailTemplate Where Name = 'Incoming Lead Call Ignore Status Email'];
        List<Contact> con = [Select Id, Email From Contact Where Email != null Limit 1];
        
        for(Lead_Allocation__c listLA : newList) {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTargetObjectId(con[0].Id);
            mail.setTemplateId(templateId.Id);
            mail.setSaveAsActivity(false);
            mail.setWhatId(listLA.ID);
            lstMsg.add(mail);
            mapOfLeadAllocationRecord.put(listLA.Id,mail);
        }
        
        Savepoint sp = Database.setSavepoint();
        Messaging.sendEmail(lstMsg);
        Database.rollback(sp);
        
        List<Messaging.SingleEmailMessage> msgListToBeSend = new List<Messaging.SingleEmailMessage>();
                
        for(Lead_Allocation__c la: newList) {   
            if(la.Lead_Ignore_Time__c != mapOldRecord.get(la.ID).Lead_Ignore_Time__c) {
                if(la.Agent__c != null) {
                    emailIdsList.add(la.Agent__r.Email);
                }
                
                if(la.Agent__c != null && la.Agent__r.ManagerId != null) {
                    emailIdsList.add(la.Agent__r.Manager.Email);
                }
                
                
                if(emailIdsList != null && emailIdsList.size() > 0) {
                    Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
                    if(mapOfLeadAllocationRecord.containsKey(la.Id)) {
                        emailToSend.setToAddresses(emailIdsList);
                        emailToSend.setPlainTextBody(mapOfLeadAllocationRecord.get(la.Id).getPlainTextBody());
                        emailToSend.setHTMLBody(mapOfLeadAllocationRecord.get(la.Id).getHTMLBody());
                        emailToSend.setSubject(mapOfLeadAllocationRecord.get(la.Id).getSubject());
                        msgListToBeSend.add(emailToSend);
                    }
                }
            }
        }
       
        if(msgListToBeSend.size() > 0) {
            Messaging.sendEmail(msgListToBeSend);
        }
    }
}