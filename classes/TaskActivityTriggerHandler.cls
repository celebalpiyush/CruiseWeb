public class TaskActivityTriggerHandler {
    
    public static void afterInsert(List<Task> newList) {
        leadActivityStatus(newList);
    }
    public static void afterUpdate(List<Task> newList) {
        leadActivityStatus(newList);
    }
    
    public static void leadActivityStatus(List<Task> ListNewUpdate) {
        
        List<Task> newList = [SELECT Id, Subject, WhoId, whatId, Status, Who.Name FROM Task WHERE Id IN :ListNewUpdate];
        List<Task> emailTasks = new List<Task>();
        List<Task> phoneTasks = new List<Task>();
        Set<String> setOfPhoneNumbers = new Set<String>();
        Map<String, Task> mapPhoneNumberToActivity = new Map<String, Task>();
        Set<Id> setOfUser = new Set<Id>();
        List<User> usersList = new List<User>();
        for(Task updatedTask : newList) {
            if(updatedTask.Subject.startswith('Call') && updatedTask.Status == 'Completed'){
                String phNumberToConvert = updatedTask.Subject;
                if(phNumberToConvert != null){
                    phNumberToConvert= phNumberToConvert.replaceAll('[^0-9]', '');
                }
                if(phNumberToConvert.length() < 10) {
                    continue;
                }
                String phoneNumber = phNumberToConvert.substring(phNumberToConvert.length() - 10, phNumberToConvert.length());
                if(phoneNumber.isNumeric()) {   
                    phoneTasks.add(updatedTask);
                }
            } else {
                if((updatedTask.Status == 'Completed' || updatedTask.Status == 'Sent')  && updatedTask.Subject.contains('Your Cruise Request')){
                    emailTasks.add(updatedTask);
                }
            }
            
        }
        
        if(phoneTasks.size() > 0){
            for(Task updatedTask : phoneTasks) {
                String phNumberToConvert = updatedTask.Subject;
                if(phNumberToConvert != null){
                    phNumberToConvert= phNumberToConvert.replaceAll('[^0-9]', '');
                    String phoneNumber = phNumberToConvert.substring(phNumberToConvert.length() - 10, phNumberToConvert.length());
                    setOfPhoneNumbers.add(phoneNumber);
                    mapPhoneNumberToActivity.put(phoneNumber, updatedTask);
                }
                
            }
            Set<String> setOfLeadPhones = new Set<String>();
            Map<String, Agent_Availability_Matrix__c> updateAgentAvailablityMatrixs = new Map<String, Agent_Availability_Matrix__c>();
            for(Agent_Availability_Matrix__c agentAvailablityMatrix : [SELECT Id, Agent__c, Lead__c, Lead__r.Phone__c FROM Agent_Availability_Matrix__c WHERE Event__c = 'Occupied' AND End_Time__c = Null]){
                setOfLeadPhones.add(agentAvailablityMatrix.Lead__r.Phone__c);
                updateAgentAvailablityMatrixs.put(agentAvailablityMatrix.Lead__r.Phone__c, agentAvailablityMatrix);
            }
            
            List<Lead_Allocation__c> leadAllocations = [SELECT Id, Agent__c, Lead__c, Lead__r.Phone__c FROM Lead_Allocation__c WHERE Lead__r.Phone__c IN: setOfPhoneNumbers and status__c = 'Accepted' ];
            for(Lead_Allocation__c lstLA: leadAllocations) {  
                lstLA.status__c = 'Acted';
                lstLA.Lead_Acted_Time__c = System.Now();
                lstLA.Completed_Activity_Id__c = mapPhoneNumberToActivity.get(lstLA.Lead__r.Phone__c).Id;
                lstLA.Completed_Activity_Name__c = mapPhoneNumberToActivity.get(lstLA.Lead__r.Phone__c).Subject;
            }
            
            List<Agent_Availability_Matrix__c> updateMatrix = new List<Agent_Availability_Matrix__c>();
            if(setOfPhoneNumbers.size() > 0) {
                for(Lead_Allocation__c lstLA: [SELECT Id, Agent__c, Lead__r.Phone__c FROM Lead_Allocation__c WHERE Lead__r.Phone__c IN: setOfLeadPhones]) {  
                    if(updateAgentAvailablityMatrixs.containsKey(lstLA.Lead__r.Phone__c)){
                        setOfUser.add(lstLA.Agent__c); 
                        Agent_Availability_Matrix__c agentAvailabilityMatrix = updateAgentAvailablityMatrixs.get(lstLA.Lead__r.Phone__c);
                        agentAvailabilityMatrix.End_Time__c = System.now();
                        updateMatrix.add(agentAvailabilityMatrix);
                        update agentAvailabilityMatrix;
                        Agent_Availability_Matrix__c agentAvailabilityMatrix2 = new Agent_Availability_Matrix__c();
                        agentAvailabilityMatrix2.Agent__c = UserInfo.getUserId();
                        agentAvailabilityMatrix2.Start_Time__c = System.now();
                        agentAvailabilityMatrix2.Event__c = 'Available';
                        updateMatrix.add(agentAvailabilityMatrix2);
                        insert agentAvailabilityMatrix2;
                    }
                }
                
                for(User userData : [SELECT Id, Is_Available__c FROM User WHERE Id IN: setOfUser]) {
                    userData.Is_Available__c= true;
                    usersList.add(userData);
                }
                if(usersList.size() > 0){
                    update usersList;    
                }
                update leadAllocations;
                
                // upsert updateMatrix;
            } 
        }
        
        if(emailTasks.size() > 0){
            Map<Id, Task> mapActivityType = new Map<Id, Task>();
            Set<Id> setLeadId = new Set<Id>();
            Set<Id> whatIDs = new Set<Id>();
            for(Task updatedTask : emailTasks) {
                whatIDs.add(updatedTask.whatId);
                mapActivityType.put(updatedTask.whatId, updatedTask);   
            }
            Map<String, Agent_Availability_Matrix__c> updateAgentAvailablityMatrixs = new Map<String, Agent_Availability_Matrix__c>();
            for(Agent_Availability_Matrix__c agentAvailablityMatrix : [SELECT Id, Agent__c, Lead__c, Lead__r.Phone FROM Agent_Availability_Matrix__c WHERE Event__c = 'Occupied' AND End_Time__c = Null]){
                    setLeadId.add(agentAvailablityMatrix.Lead__c);
                    updateAgentAvailablityMatrixs.put(agentAvailablityMatrix.Lead__c, agentAvailablityMatrix);
            }
            Set<Id> leadIDs = new Set<Id>();
            Map<Id, Id> mapLeadIdToAccountId = new Map<Id, Id>();
            system.debug(whatIDs);
            for(Lead ld : [Select Id, ConvertedOpportunityId, ConvertedAccountId From Lead where ConvertedAccountId IN :whatIDs]){
                leadIDs.add(ld.Id);
                mapLeadIdToAccountId.put(ld.Id, ld.ConvertedAccountId);
            }
            List<Lead_Allocation__c> leadAllocations = [SELECT Id, Agent__c, Lead__c FROM Lead_Allocation__c WHERE Lead__c IN: leadIDs];
            for(Lead_Allocation__c lstLA: leadAllocations) {  
                lstLA.status__c = 'Acted';
                lstLA.Lead_Acted_Time__c = System.Now();
                lstLA.Completed_Activity_Id__c = mapActivityType.get(mapLeadIdToAccountId.get(lstLA.Lead__c)).Id;
                lstLA.Completed_Activity_Name__c = mapActivityType.get(mapLeadIdToAccountId.get(lstLA.Lead__c)).Subject;
            }
            if(whatIDs.size() > 0) {
                List<Agent_Availability_Matrix__c> updateMatrix = new List<Agent_Availability_Matrix__c>();
                for(Lead_Allocation__c lstLA: [SELECT Id, Agent__c, Lead__c FROM Lead_Allocation__c WHERE Lead__c IN: setLeadId]) {  
                    if(updateAgentAvailablityMatrixs.containsKey(lstLA.Lead__c)){
                        setOfUser.add(lstLA.Agent__c); 
                        Agent_Availability_Matrix__c agentAvailabilityMatrix = updateAgentAvailablityMatrixs.get(lstLA.Lead__c);
                        agentAvailabilityMatrix.End_Time__c = System.now();
                        updateMatrix.add(agentAvailabilityMatrix);
                        Agent_Availability_Matrix__c agentAvailabilityMatrix2 = new Agent_Availability_Matrix__c();
                        agentAvailabilityMatrix2.Agent__c = UserInfo.getUserId();
                        agentAvailabilityMatrix2.Start_Time__c = System.now();
                        agentAvailabilityMatrix2.Event__c = 'Available';
                        updateMatrix.add(agentAvailabilityMatrix2);
                    }
                }
                
                System.debug('setOfUser'+setOfUser);
                
                for(User userData : [SELECT Id, Is_Available__c FROM User WHERE Id IN: setOfUser]) {
                    userData.Is_Available__c= true;
                    usersList.add(userData);
                }
                System.debug('usersList'+usersList);
                
                if(usersList.size() > 0){
                    update usersList;    
                }
                update leadAllocations;
                
                upsert updateMatrix;
            }
        }
    }
}