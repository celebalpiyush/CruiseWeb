global class LeadAllocationScheduler implements schedulable{

    global void execute(SchedulableContext SC) {
        LeadAssignmentUtility.leadAssignment();
    }
}