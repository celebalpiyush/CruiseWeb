public class LeadAllocationAppletController {
    
    public Set<String> setOfRank = new Set<String>();
    public List<Lead> uniqueLeads = new List<Lead>();
    public Set<String> setofDuplicateLeadbyEmail = new Set<String>();
    public Set<String> setofDuplicateLeadbyPhone = new Set<String>();
    public Set<Id> setOfQueueId = new Set<Id>();
    public Map<String,List<Lead>> mapOfLeads = new Map<String,List<Lead>>();
    //public Map<String,User> mapOfUserAgents = new Map<String,User>();
    public List<Id> lstofUserAgents = new List<Id>();
    public List<Id> lstofUserAgentsSelRandom = new List<Id>();
    public List<Lead_Allocation__c> leadAllocationRecordList = new List<Lead_Allocation__c>();
    public Integer arrayLengthofUsers;
    public Boolean showPopUp {get;set;}
    public User usr {get;set;}
    public List<Lead_Allocation__c> leadAllocation;
    public Integer responseTime {get;set;}
    public String redirectURL {get {
        if(redirectURL == null){
            redirectURL = '';
        } 
        return redirectURL;
        } set;}
    
    
    public LeadAllocationAppletController() {
        usr = [SELECT Id, Is_Available__c FROM User WHERE Id = :UserInfo.getUserId()];
        showPopUp = false;
    }
    
    public void runSchedule() {
        LeadAssignmentUtility.leadAssignment();
    }
    
    public Lead_Allocation__c leadalloc ;
    
    public void updateLeadPresented(){
        usr = [SELECT Id, Is_Available__c FROM User WHERE Id = :UserInfo.getUserId()];
        usr.Is_Available__c = false;
        update usr;
        
        if (leadAlloc != null){
            leadAlloc.Status__c  = 'Presented';
            leadAlloc.Lead_Presented_Time__c = system.now();
            update leadAlloc; 
            leadAlloc = null;
        }
        
        
    }
    
    public void getLeadAllocation() {
        usr = [SELECT Id, Is_Available__c FROM User WHERE Id = :UserInfo.getUserId()];
        usr.Last_Action_Poller_Request__c = System.now();
        update usr;
        if(usr.Is_Available__c && !showPopUp) {
            List<Lead_Allocation__c> leads = [SELECT id,Status__c, Lead__c, Lead__r.OwnerId, Lead__r.Name, Lead__r.Phone, Lead__r.Email, createdDate, Lead__r.FirstName FROM Lead_Allocation__c WHERE (Status__c = 'Allocated') AND Agent__c =: UserInfo.getUserId() ORDER BY createdDate DESC];
            
            Map<Decimal, Queue_Ranking__mdt> mapQueueRanking = new Map<Decimal, Queue_Ranking__mdt>();
            Set<String> queueNames = new Set<String>();
            for(Queue_Ranking__mdt qRank :[SELECT Id, Ranking__c,Label FROM Queue_Ranking__mdt ORDER BY Ranking__c ASC]) {
                mapQueueRanking.put(qRank.Ranking__c, qRank);
                queueNames.add(qRank.Label);
            }
            
            Map<String, Id> mapQueueNameToId = new Map<String, Id>();
            for(Group grp : [SELECT Id,Name FROM Group WHERE Name IN :queueNames AND Type = 'Queue']) {
                mapQueueNameToId.put(grp.Name, grp.Id);            
            }
            
            leadAllocation = new List<Lead_Allocation__c>();
            List<Decimal> rankings = new List<Decimal>(mapQueueRanking.keySet());
            rankings.sort();
            for(Decimal rank : rankings) {
                for(Lead_Allocation__c lead : leads) {
                    if(lead.Lead__r.OwnerId == mapQueueNameToId.get(mapQueueRanking.get(rank).Label)) {
                        leadAllocation.add(lead);
                        break;
                    }
                }
            }
            
            if(leadAllocation.size() == 0) {
               // update usr;
                return;
            }
            
            List<Lead_Allocation__c> actedLeads = [SELECT Id, Lead_Acted_Time__c FROM Lead_Allocation__c WHERE Status__c = 'Acted' AND Agent__c =: UserInfo.getUserId() ORDER BY Lead_Acted_Time__c DESC LIMIT 1];
            if(actedLeads != null && actedLeads.size() > 0 && actedLeads[0].Lead_Acted_Time__c != null) {
                if(System.now().getTime() - actedLeads[0].Lead_Acted_Time__c.getTime() < 30 * 1000) {
                    return;
                }
            }
            System.debug('leadAllocation1'+leadAllocation);
            if(leadAllocation != null && leadAllocation.size() > 0 ) {
                System.debug('leadAllocation2'+leadAllocation);
                showPopUp = true;
                // usr.Is_Available__c = false;
                
                List<Agent_Response_Time__mdt> agentResponseTime = [SELECT DeveloperName,Id,Response_Time__c FROM Agent_Response_Time__mdt WHERE DeveloperName = 'Response_Time' LIMIT 1];
                
                if(agentResponseTime != null && agentResponseTime.size() > 0) {
                    System.debug('leadAllocation3'+leadAllocation);
                    responseTime = Integer.valueOf(agentResponseTime[0].Response_Time__c) * 1000;
                } else {
                    responseTime = 10000;
                }
                leadAlloc = leadAllocation[0];
                System.debug('leadAllocation4'+leadAllocation);
                // leadAllocation[0].Status__c  = 'Presented';
                // leadAllocation[0].Lead_Presented_Time__c = system.now();
                // update leadAllocation; 
            } else {
                showPopUp = false;
            }
        } else {
            showPopUp = false;
        }
        update usr;
    }
    
    public void updateLeadAllocation() {
        System.debug('leadAllocation7'+leadAllocation);
        redirectURL = '';
        system.debug('leadAllocation8'+leadAllocation);
        if(leadAllocation != null && leadAllocation.size() > 0){
            String status = [Select Status__c from Lead_Allocation__c where Id = :leadallocation[0].Id].Status__c;
            if( status != 'Accepted' && status != 'Ignored'){
                usr.Is_Available__c = false;
                update usr;
                Lead_Allocation__c lead = new Lead_Allocation__c();
                lead.id = leadAllocation[0].id;
                lead.Status__c  = 'Accepted';
                lead.Lead_Accepted_Time__c = system.now();
                update lead;
                
                Id leadId = leadAllocation[0].Lead__c;
                Lead ldx = new Lead(Id = leadId);
                if(leadAllocation[0].Lead__r.FirstName == null || leadAllocation[0].Lead__r.FirstName == 'null'){
                    ldx.FirstName = ' ';
                }
                ldx.ownerId = UserInfo.getUserID();
                update ldx;
                system.debug(leadId);
                
                
                Lead ld = [Select Id, Name, LastName, Email, Phone From Lead where Id = :leadId];
                system.debug('ld'+ld);
                String email = ld.Email;
                String phone = ld.Phone;
                String lastName = ld.LastName;
                String Name = ld.Name;
                system.debug('lastName' + lastName);
                system.debug('email' + email);
                List<Lead> leads = [SELECT Id, LastName, Email, Phone FROM Lead WHERE ((Email = :email AND Email != null) OR (Phone = :phone AND Phone != null AND LastName = :lastName)) AND isConverted = false];
                if(leads !=null && leads.size() > 1) {
                    // redirect to duplicate page
                    redirectURL = '/lead/leadmergewizard.jsp?id='+leadId;
                } else {
                    List<Account> accounts = [SELECT Id, Name, PersonEmail, Phone FROM Account WHERE ((Name = :Name AND PersonEmail = :email AND PersonEmail != null) OR (Phone = :phone AND Phone != null AND Name = :Name)) AND isPersonAccount = true];
                    system.debug('accounts'+accounts);
                    if(accounts != null && accounts.size() == 1){
                        Database.LeadConvert lc = new database.LeadConvert();
                        lc.setLeadId(ld.id);
                        lc.ConvertedStatus = 'Assigned Client';
                        lc.setAccountId(accounts[0].Id);
                        lc.setOwnerId(UserInfo.getUserId());
                        Database.LeadConvertResult lcr = Database.convertLead(lc);   
                        Lead ldx2 = [SELECT ConvertedOpportunityId, ConvertedAccountId FROM Lead WHERE Id = :leadId];
                        Account convertedAccount =  [SELECT Id, OwnerId FROM Account WHERE Id = :ldx2.ConvertedAccountId];
                        convertedAccount.OwnerId = UserInfo.getUserId();
                        update convertedAccount;
                        Opportunity opp = [SELECT Id FROM Opportunity WHERE Id = :ldx2.ConvertedOpportunityId];
                        redirectURL = '/'+opp.Id;
                    }  else {
                        List<Account> accountsx = [SELECT Id, LastName, PersonEmail, Phone FROM Account WHERE ((PersonEmail = :email AND PersonEmail != null) OR (Phone = :phone AND Phone != null AND LastName = :lastName)) AND isPersonAccount = true];
                        if(accountsx != null && accountsx.size() >= 1){
                             // redirect to duplicate page
                            redirectURL = '/lead/leadmergewizard.jsp?id='+leadId;   
                        } else {
                            Database.LeadConvert lc = new database.LeadConvert();
                            lc.setLeadId(ld.id);
                            lc.ConvertedStatus = 'Assigned Client';
                            lc.setOwnerId(UserInfo.getUserId());
                            Database.LeadConvertResult lcr = Database.convertLead(lc); 
                            Lead ldx2 = [SELECT ConvertedOpportunityId FROM Lead WHERE Id = :leadId];
                            Opportunity opp = [SELECT Id FROM Opportunity WHERE Id = :ldx2.ConvertedOpportunityId];
                            redirectURL = '/'+opp.Id;
                        }
                    }
                } 
                showPopUp = false; 
                List<Agent_Availability_Matrix__c> agentAvailablityMatrix = [Select Id From Agent_Availability_Matrix__c where Agent__c = :UserInfo.getUserId() order by createdDate desc Limit 1];
                if(agentAvailablityMatrix != null && agentAvailablityMatrix.size() > 0){
                    agentAvailablityMatrix[0].End_Time__c = System.Now();
                    update agentAvailablityMatrix;
                }
                
                Agent_Availability_Matrix__c agentAvailabilityMatrix2 = new Agent_Availability_Matrix__c();
                agentAvailabilityMatrix2.Agent__c = UserInfo.getUserId();
                agentAvailabilityMatrix2.Start_Time__c = System.now();
                agentAvailabilityMatrix2.Event__c = 'Occupied';
                agentAvailabilityMatrix2.Lead__c = leadId;
                insert agentAvailabilityMatrix2;
            }
        }
    }
    public void ignoreLeadAllocation(){
        
        if(leadAllocation != null &&  leadAllocation.size() > 0){
            String status = [Select Status__c from Lead_Allocation__c where Id = :leadallocation[0].Id].Status__c;
            if( status != 'Accepted' && status != 'Ignored'){
                showPopUp = false; 
                usr.Is_Available__c = false;
                update usr;
                Lead_Allocation__c lead = new Lead_Allocation__c();
                lead.id = leadAllocation[0].id;
                lead.Status__c  = 'Ignored';
                lead.Lead_Ignore_Time__c = system.now();
                update lead;
                List<Agent_Availability_Matrix__c> agentAvailablityMatrix = [Select Id From Agent_Availability_Matrix__c where Agent__c = :UserInfo.getUserId() order by createdDate desc Limit 1];
                if(agentAvailablityMatrix != null && agentAvailablityMatrix.size() > 0){
                    agentAvailablityMatrix[0].End_Time__c = System.Now();
                    update agentAvailablityMatrix;
                }
                
                Agent_Availability_Matrix__c agentAvailabilityMatrix2 = new Agent_Availability_Matrix__c();
                agentAvailabilityMatrix2.Agent__c = UserInfo.getUserId();
                agentAvailabilityMatrix2.Start_Time__c = System.now();
                agentAvailabilityMatrix2.Event__c = 'Unavailable (System)';
                
                insert agentAvailabilityMatrix2;
            }
        }
    }
    //Action poller code ends
    
    public void changeUserStatus(){
        usr = [Select Id, Is_Available__c From User where Id = :UserInfo.getUserId()];
        usr.Is_Available__c = !usr.Is_Available__c;
        update usr;
        
        List<Agent_Availability_Matrix__c> agentAvailablityMatrix = [Select Id From Agent_Availability_Matrix__c where Agent__c = :UserInfo.getUserId() order by createdDate desc Limit 1];
        if(agentAvailablityMatrix != null && agentAvailablityMatrix.size() > 0){
            agentAvailablityMatrix[0].End_Time__c = System.Now();
            update agentAvailablityMatrix;
        }
        
        Agent_Availability_Matrix__c agentAvailabilityMatrix2 = new Agent_Availability_Matrix__c();
        agentAvailabilityMatrix2.Agent__c = UserInfo.getUserId();
        agentAvailabilityMatrix2.Start_Time__c = System.now();
        if(usr.Is_Available__c){
            agentAvailabilityMatrix2.Event__c = 'Available';
        } else {
            agentAvailabilityMatrix2.Event__c = 'Unavailable (User)';
        }
        insert agentAvailabilityMatrix2;
    }
}