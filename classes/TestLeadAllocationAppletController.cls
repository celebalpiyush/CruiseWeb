@istest
private class TestLeadAllocationAppletController {    
    static testMethod void leadUnitTest1(){
        
        Id leadRt = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Queue Lead Record Type').getRecordTypeId();
        
        Account acc= new Account();
        acc.Name = 'test';
       // acc.PersonEmail = 'test@gmail.com';
        acc.Phone = '123456789';
        insert acc;
        
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles',
                          UserName=uniqueUserName,
                          Is_Available__c = true);
        insert u;
        
        System.runAs(u) {          
            Lead leadData = new Lead();
            leadData.Company = 'test';
            leadData.LastName = 'testLastName';
            leadData.Phone = '123445678';
            leadData.Email = 'test@gmail.com';
            //leadData. 
            insert leadData;
            
            Lead_Allocation__c leadA = new Lead_Allocation__c();
            leadA.Status__c = 'Allocated';
            leadA.Agent__c = u.Id;
            leadA.Lead__c = leadData.Id;
            insert leadA;
            
            Agent_Availability_Matrix__c agentM = new Agent_Availability_Matrix__c();
            agentM.Agent__c = u.Id;
            agentM.Name = 'test';
            agentM.Lead__c = leadData.Id;
            agentM.Event__c = 'Occupied';
            insert agentM;
            
            LeadAllocationAppletController ctrl = new LeadAllocationAppletController();
            ctrl.getLeadAllocation();
            ctrl.ignoreLeadAllocation();
            ctrl.changeUserStatus();
        }
    }
    static testMethod void leadUnitTest2(){
        
        Id leadRt = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Queue Lead Record Type').getRecordTypeId();
        
        Account acc= new Account();
        acc.Name ='test 0011';
        acc.Type = 'Acquired';
       
        insert acc;
        
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles',
                          UserName=uniqueUserName,
                          Is_Available__c = true);
        insert u;
        
        System.runAs(u) {          
            Lead leadData = new Lead(Company = 'Test Lead2',LastName = 'Lead Last Name2', RecordTypeId = leadRt);
            insert leadData;
            
            Lead_Allocation__c leadA = new Lead_Allocation__c();
            leadA.Status__c = 'Accepted';
            leadA.Agent__c = u.Id;
            leadA.Lead__c = leadData.Id;
            insert leadA;
            
            Agent_Availability_Matrix__c agentM = new Agent_Availability_Matrix__c();
            agentM.Agent__c = u.Id;
            agentM.Name = 'test';
            agentM.Lead__c = leadData.Id;
            agentM.Event__c = 'Occupied';
            insert agentM;
            
            LeadAllocationAppletController ctrl = new LeadAllocationAppletController();
            ctrl.getLeadAllocation();
            ctrl.updateLeadAllocation();
            ctrl.ignoreLeadAllocation();
            ctrl.changeUserStatus();
        }
    }
    static testMethod void updateLeadAllocationTest(){
        RecordType accRT = [SELECT DeveloperName,Id,Name FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Business_Account' limit 1];
        Account acc= new Account();
        acc.Name = 'test';
       // acc.PersonEmail = 'test@gmail.com';
        acc.Phone = '123456789';
        acc.RecordTypeId = accRT.Id;
        insert acc;
        
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles',
                          UserName=uniqueUserName,
                          Is_Available__c = true);
        insert u;
        Id leadRt = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Queue Lead Record Type').getRecordTypeId();
        RecordType rt = [SELECT DeveloperName,Id,Name FROM RecordType WHERE SobjectType = 'Lead' AND DeveloperName = 'Standard_Lead_Record_Type' limit 1];
        
        
        system.runAs(u){
            Lead leadData = new Lead(Company = 'Test Lead',LastName = 'Lead Last Name', RecordTypeId = rt.Id);
            insert leadData;
            System.debug('leadData'+leadData);
            Lead_Allocation__c leadA = new Lead_Allocation__c();
            leadA.Status__c = 'Allocated';
            leadA.Agent__c = u.Id;
            leadA.Lead__c = leadData.Id;
            insert leadA;
            LeadAllocationAppletController ctrl = new LeadAllocationAppletController();
            ctrl.getLeadAllocation();
            ctrl.updateLeadAllocation();
        }
    }
}