@isTest
public class LeadOwnerAssignmentTest {
    
    static testMethod void matchingPhoneNumberCases(){
        
        //-- Insert Personal Account Records
        RecordType personAccountRecordType =  [SELECT Id FROM RecordType WHERE Name = 'Person Account' and SObjectType = 'Account'];
        Account newPersonAccount = new Account();       
        
        Account newAccount = new Account();
        newAccount.FirstName ='Test';
        newAccount.LastName ='Account';
        newAccount.PersonEmail='invalidemailnotmatching@gmail.com';
        newAccount.Phone='8989898989';
        newAccount.RecordType = personAccountRecordType;
        insert newAccount;         
        
        Lead newLead = new Lead();
        newLead.firstName='Test';
        newLead.lastName='Lead';
        newLead.Phone='8989898989';
        newLead.Email='emailnotmatching@gmail.com';
        newLead.LeadSource='Website Lead';
        
        Test.startTest();
        insert newLead;
        Test.stopTest();       
        
    }
    
    static testMethod void matchingFaxNumberCases(){
        
        //-- Insert Personal Account Records
        RecordType personAccountRecordType =  [SELECT Id FROM RecordType WHERE Name = 'Person Account' and SObjectType = 'Account'];
        Account newPersonAccount = new Account();       
        
        Account newAccount = new Account();
        newAccount.FirstName ='Test';
        newAccount.LastName ='Account';
        newAccount.PersonEmail='invalidemailnotmatching@gmail.com';
        newAccount.Phone='8989898989';
        newAccount.RecordType = personAccountRecordType;
        insert newAccount;         
        
        Lead newLead = new Lead();
        newLead.firstName='Test';
        newLead.lastName='Lead';
        newLead.Phone='89898989891';
        newLead.Fax='89898989891';
        newLead.Email='emailnotmatching@gmail.com';
        newLead.LeadSource='Website Lead';
        
        Test.startTest();
        insert newLead;
        Test.stopTest();
    }
    
    
    static testMethod void runPositiveTestCases(){
        
        List<LeadOwnerAssignmentInvalidPhoneList__c> LeadOwnerAssignmentInvalidPhoneObjectList = new List<LeadOwnerAssignmentInvalidPhoneList__c>();  
        for(integer i=0;i<5;i++){  
            
            LeadOwnerAssignmentInvalidPhoneList__c LeadOwnerAssignmentInvalidPhoneObj = new LeadOwnerAssignmentInvalidPhoneList__c();
        	LeadOwnerAssignmentInvalidPhoneObj.Name='1111111111'+i;
            LeadOwnerAssignmentInvalidPhoneObj.PhoneNumber__c='1111111111'+i;
            LeadOwnerAssignmentInvalidPhoneObjectList.add(LeadOwnerAssignmentInvalidPhoneObj);
        }
        
        if(LeadOwnerAssignmentInvalidPhoneObjectList!=null && LeadOwnerAssignmentInvalidPhoneObjectList.size()>0){
            insert LeadOwnerAssignmentInvalidPhoneObjectList;
        }
       
        
        //-- Insert Personal Account Records
        RecordType personAccountRecordType =  [SELECT Id FROM RecordType WHERE Name = 'Person Account' and SObjectType = 'Account'];
		Account newPersonAccount = new Account(); 

		List<Account> personalAccountList = new List<Account>();        
        for(integer i=0;i<5;i++){            
            Account newAccount = new Account();
            newAccount.FirstName ='Test';
            newAccount.LastName ='Account'+i;
            newAccount.PersonEmail='test'+i+'@gmail.com';
            newAccount.RecordType = personAccountRecordType;
            personalAccountList.add(newAccount);            
        }
        
        for(integer k=0;k<5;k++){            
            Account newAccount = new Account();
            newAccount.FirstName ='Test';
            newAccount.LastName ='Account'+k;
            newAccount.Phone ='111111111'+k;
             newAccount.Fax ='222222222'+k;
            newAccount.PersonEmail='invalid'+k+'@gmail.com';
            newAccount.RecordType = personAccountRecordType;
            personalAccountList.add(newAccount);            
        }
        
        
      
        
        //Test.startTest();
        insert personalAccountList;
       // Test.stopTest();
        
        //-- Insert Lead records
        List<Lead> leadList = new List<Lead>();
        for(integer i=0;i<5;i++){
            
            Lead newLead = new Lead();
            newLead.firstName='Test';
            newLead.lastName='Lead'+i;
            newLead.Phone='000'+i;
            newLead.Email='test'+i+'@gmail.com';
            newLead.LeadSource='Website Lead';
            leadList.add(newLead);
        }
                
         for(integer i=0;i<30;i++){
            
            Lead newLead = new Lead();
            newLead.firstName='Test';
            newLead.lastName='Lead'+i;
            newLead.Phone='1111111111'+i;             
            newLead.Email='testP'+i+'@gmail.com';
            newLead.LeadSource='Website Lead';
            leadList.add(newLead);
        }
       
    
         for(integer i=0;i<20;i++){
            
            Lead newLead = new Lead();
            newLead.firstName='Test';
            newLead.lastName='Lead'+i;
            newLead.Phone='2222222222'+i;
            newLead.Email='testP'+i+'@gmail.com';
            newLead.LeadSource='Website Lead';
            leadList.add(newLead);
        }
         
        Test.startTest();
        	insert leadList;
        Test.stopTest();
        
        
        
    }

}