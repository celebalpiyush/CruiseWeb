public class LeadAssignmentUtility {
    public static void leadAssignment(){
        
        // Note : Important step : This method is being called from two places
        // 1) Scheduler runs this method every n minutes.
        // 2) This method is triggered when any task completed.
        // So we are avoiding a conflict if at the same instant it runs from two places with the help of custom setting.
        
        Is_Scheduler_Running__c isSchedulerRunning = Is_Scheduler_Running__c.getValues('Lead Allocation Scheduler');
         if(isSchedulerRunning != null){
                isSchedulerRunning.Is_Running__c = false;
                update isSchedulerRunning;
            }
        if(isSchedulerRunning != null && isSchedulerRunning.Is_Running__c){
            return;
        }
        if(isSchedulerRunning == null){
            isSchedulerRunning = new Is_Scheduler_Running__c();
            isSchedulerRunning.Name = 'Lead Allocation Scheduler';
            isSchedulerRunning.Is_Running__c = true;
            insert isSchedulerRunning;
        }
        if(isSchedulerRunning != null){
            isSchedulerRunning.Is_Running__c = true;
            update isSchedulerRunning;
        }
        
        // Change Request : Need to disable the users whose pooler request has not been made from last one minute
        
        List<User> users = [Select Id From User where Last_Action_Poller_Request__c < :System.Now().addMinutes(-1)];
        Set<Id> userIDsx = new Set<Id>();
        for(User usr : users){
            usr.Is_Available__c = false;
            userIDsx.add(usr.Id);
        }
        
        List<Agent_Availability_Matrix__c> agentAvailabilityMatrixs = new List<Agent_Availability_Matrix__c>();
        Map<Id, Agent_Availability_Matrix__c> mapAgentAvailablityMatrix = new Map<Id,Agent_Availability_Matrix__c>();
        for(Agent_Availability_Matrix__c agentAvailabilityMatrix : [Select Id, Agent__c From Agent_Availability_Matrix__c where Agent__c = :userIDsx order by createdDate]){
            if(!mapAgentAvailablityMatrix.containsKey(agentAvailabilityMatrix.Agent__c)){
                mapAgentAvailablityMatrix.put(agentAvailabilityMatrix.Agent__c, agentAvailabilityMatrix);
            }
        }
        
        for(Agent_Availability_Matrix__c matrix : mapAgentAvailablityMatrix.values()){
            Agent_Availability_Matrix__c aam = matrix;
            aam.End_Time__c = System.Now();
            agentAvailabilityMatrixs.add(aam);
            Agent_Availability_Matrix__c agentAvailabilityMatrix2 = new Agent_Availability_Matrix__c();
            agentAvailabilityMatrix2.Agent__c = matrix.Agent__c;
            agentAvailabilityMatrix2.Start_Time__c = System.now();
            agentAvailabilityMatrix2.Event__c = 'Unavailable (System)';
            agentAvailabilityMatrixs.add(agentAvailabilityMatrix2);
        }
        
        update users;
        upsert agentAvailabilityMatrixs;
        
        //Step1 : Get all the leads which are New or ignored
        // Get actual leads from lead allocation object as well.
        Set<Id> leadIDs = new Set<Id>();
        Map<Id, Lead_Allocation__c> mapLeadIDToLeadAllocation = new Map<Id, Lead_Allocation__c>();
        for (Lead_Allocation__c lead : [Select Id, Lead__c, Lead__r.OwnerId, Lead__r.Name, Lead__r.Phone, Lead__r.Email, createdDate From Lead_Allocation__c where Status__c = 'New' Or Status__c = 'Ignored' Or Status__c = 'Allocated' Or (Status__c = 'Presented' AND Lead_Presented_Time__c < :System.Now().addSeconds(-30)) ORDER By CreatedDate DESC]){
            mapLeadIDToLeadAllocation.put(lead.Lead__c, lead);
            System.debug('mapLeadIDToLeadAllocation'+mapLeadIDToLeadAllocation);
            leadIDs.add(lead.Lead__c);
            System.debug('leadIDs'+leadIDs);
        }
        
        // Getting the lead information which will be used later in the algorithm
        Map<Id, Lead> mapLead = new Map<Id, Lead>([Select Id, City, Country, State, CreatedDate From Lead where Id IN :leadIds]);
        
        //Step2 : Get the timeZoneInformation and exclude which does not meet the criteria
        // 1) Lead should be in the time of 15 minutes of Lead creation
        // or 2 ) Lead should be acted only between 9:00 Am to 8:45 PM 
        Map<String, Timezone_Information__c> mapLocationToTimeZone = new Map<String, Timezone_Information__c>();
        Map<String, Lead> updatedLeadMap = new Map<String, Lead>();
        
        for(Timezone_Information__c timeZoneInformation : [Select Id, Name, Timezone__c From Timezone_Information__c]){
            mapLocationToTimeZone.put(timeZoneInformation.Name, timeZoneInformation);
        }
        System.debug('mapLeadIDToLeadAllocation2'+mapLeadIDToLeadAllocation);
        for(Lead_Allocation__c lead : mapLeadIDToLeadAllocation.values()){
            if(lead.Lead__c == null){
                continue;
            }
            DateTime createdDateTime = mapLead.get(lead.Lead__c).createdDate;
            Timezone tz;
            String location = '';
            if(mapLead.get(lead.Lead__c).Country == 'United States' || mapLead.get(lead.Lead__c).Country == 'USA' || mapLead.get(lead.Lead__c).Country == 'US'){
                if(mapLead.get(lead.Lead__c).State == null || mapLead.get(lead.Lead__c).State == ''){
                    location = mapLead.get(lead.Lead__c).City;
                } else {
                    location = mapLead.get(lead.Lead__c).State;
                }
            } else if(mapLead.get(lead.Lead__c).Country == '' || mapLead.get(lead.Lead__c).Country == null){
                location = mapLead.get(lead.Lead__c).City;
            } else {
                location = mapLead.get(lead.Lead__c).Country;
            }
            if(location == null || mapLocationToTimeZone.get(location) == null) {
                system.debug('America/New_York');
                tz = Timezone.getTimeZone('America/New_York');
            } else {
                tz = Timezone.getTimeZone(mapLocationToTimeZone.get(location).Timezone__c);
            }
            system.debug(createdDateTime);
            system.debug(lead.Lead__c);
            createdDateTime = createdDateTime.addSeconds(tz.getOffset(createdDateTime) / 1000);
            system.debug(createdDateTime);
            system.debug(System.now().getTime() - mapLead.get(lead.Lead__c).createdDate.getTime());
            
            DateTime timeNowAtLead = System.now().addSeconds(tz.getOffset(System.now()) / 1000);
            system.debug(timeNowAtLead);
            if((System.now().getTime() - mapLead.get(lead.Lead__c).createdDate.getTime()) < 15*1000 * 60 || (timeNowAtLead.hourGMT() >= 9 && timeNowAtLead.hourGMT() < 20) || (timeNowAtLead.hourGMT() == 20 && (timeNowAtLead.minuteGMT() >= 0 && timeNowAtLead.minuteGMT() <= 45 ))){
                system.debug(System.now().getTime() - mapLead.get(lead.Lead__c).createdDate.getTime());
                system.debug(createdDateTime.hourGMT());
                system.debug(createdDateTime.minuteGMT());
                updatedLeadMap.put(lead.Lead__c, mapLead.get(lead.Lead__c));
            }
        }
        system.debug(updatedLeadMap);
        // Step3: Get the ranking of the queues 
        // Loop to fetch the ranking of the desired queues
        Map<Decimal, Queue_Ranking__mdt> mapQueueRanking = new Map<Decimal, Queue_Ranking__mdt>();
        Set<String> queueNames = new Set<String>();
        for(Queue_Ranking__mdt qRank :[SELECT Id, Ranking__c,Label FROM Queue_Ranking__mdt Order By Ranking__c ASC] ){
            mapQueueRanking.put(qRank.Ranking__c, qRank);
            queueNames.add(qRank.Label);
        }
        
        // Loop to fetch the Id of the required queues
        Map<String, Id> mapQueueNameToId = new Map<String, Id>();
        for(Group grp : [SELECT Id,Name FROM Group WHERE Name IN :queueNames And Type = 'Queue']){
            mapQueueNameToId.put(grp.Name, grp.Id);            
        }
        System.debug('mapQueueNameToId'+mapQueueNameToId);
        // Get the queue members
        Set<Id> allUserIds = new Set<Id>();
        Map<Id, List<Id>> mapQueueIdToUserIDs = new Map<Id, List<Id>>();
        Map<Id, List<Id>> mapQueueIdToUserIDs2 = new Map<Id, List<Id>>();
        for( GroupMember gm : [Select UserOrGroupId, GroupId From GroupMember where GroupId =:mapQueueNameToId.values()] ){
            if(!mapQueueIdToUserIDs.containsKey(gm.GroupID)){
                mapQueueIdToUserIDs.put(gm.GroupID, new List<Id>());
            }
            allUserIds.add(gm.UserOrGroupId);            
            mapQueueIdToUserIDs.get(gm.GroupID).add(gm.UserOrGroupId);            
        }
        System.debug('allUserIds'+allUserIds);
        System.debug('mapQueueIdToUserIDs'+mapQueueIdToUserIDs);
        Map<String, Id> mapNameToUserID = new map<String, Id>();
        Map<Id, User> mapUsers = new Map<Id, User>();
         system.debug('HERE');
        for(User usr : [Select Id, Name From User where Id IN :allUserIDs And isActive = true and Is_Available__c = true ]){
          
            mapNameToUserID.put(usr.Name, usr.Id);
            mapUsers.put(usr.Id, usr);
        }
        System.debug('mapUsers'+mapUsers);
        System.debug('mapNameToUserID'+mapNameToUserID);
        
        for(Id groupID : mapQueueIdToUserIDs.KeySet()){
            for(Id userID : mapQueueIdToUserIDs.get(groupID)){
                if(mapUsers.containsKey(userID)){ 
                    if(!mapQueueIdToUserIDs2.containsKey(GroupID)){
                        mapQueueIdToUserIDs2.put(GroupID, new List<Id>());
                    }
                    mapQueueIdToUserIDs2.get(GroupID).add(userID);
                }
            }   
        }
        if(mapQueueIdToUserIDs2.KeySet().size() == 0){
            system.debug('no user available');
            if(isSchedulerRunning != null){
                isSchedulerRunning.Is_Running__c = false;
                update isSchedulerRunning;
            }
            return;
        }
        system.debug(mapQueueIdToUserIDs2);
        // Loop to fetch the incoming unassigned leads on the basis of the ranks and the LIFO(created date) order from appropriate queues
        Map<Id, List<Lead_Allocation__c> > mapOfleads = new Map<Id, List<Lead_Allocation__c>>();
        List<Lead_Allocation__c> leadAllocationRecordList = new List<Lead_Allocation__c>();
        Set<String> setofDuplicateLeadbyEmail = new Set<String>();
        Set<String> setofDuplicateLeadbyPhone = new Set<String>();
        for (Lead_Allocation__c olead : mapLeadIDToLeadAllocation.values()) {
            if(updatedLeadMap.keySet().contains(olead.Lead__c)){
                if(!setofDuplicateLeadbyEmail.contains(olead.Lead__r.Name + '#' + olead.Lead__r.Email) && !setofDuplicateLeadbyPhone.contains(olead.Lead__r.Name + '#' + olead.Lead__r.Phone)){
                    if(!mapOfLeads.containsKey(olead.Lead__r.OwnerId)){
                        mapOfLeads.put(olead.Lead__r.OwnerId, new List<Lead_Allocation__c>());
                    }
                    setofDuplicateLeadbyPhone.add(olead.Lead__r.Name + '#' + olead.Lead__r.Phone);
                    setofDuplicateLeadbyEmail.add(olead.Lead__r.Name + '#' + olead.Lead__r.Email);
                    system.debug(olead.Lead__r.OwnerId);
                    mapOfLeads.get(olead.Lead__r.OwnerId).add(olead);
                    
                } else {
                    olead.Status__c = 'Duplicate';
                    leadAllocationRecordList.add(oLead);
                }
            }
        }
        system.debug(mapOfLeads);
        // Step4 : Getting and sorting the queues on the basis of ranink and then assigning to the users   
        List<Decimal> rankings = new List<Decimal>(mapQueueRanking.keySet());
        rankings.sort();
        
        Decimal highestRank = rankings[rankings.size() - 1];
        for(Decimal rank : rankings){
            Decimal currentRank = rank;
            system.debug(currentRank);
            if(mapQueueNameToId.get(mapQueueRanking.get(currentRank).Label) != null){
                 system.debug(mapQueueNameToId.get(mapQueueRanking.get(currentRank).Label));
                List<Lead_Allocation__c> rankedLeads = mapOfLeads.get(mapQueueNameToId.get(mapQueueRanking.get(currentRank).Label));
                // system.debug(rankedLeads.size());
                if(rankedLeads != null && rankedLeads.size() > 0){
                    Integer sizeOfRankedLeads = rankedLeads.size();
                    Integer sizeOfLeadsAssigned = 0;
                    Boolean userPresentInQueuesAhead = false;
                    while(sizeOfRankedLeads > sizeOfLeadsAssigned ){
                        system.debug('sizeOfRankedLeads' + sizeOfRankedLeads);
                        system.debug('sizeOfLeadsAssigned' + sizeOfLeadsAssigned);
                        system.debug('currentRank' + currentRank);
                        system.debug(mapQueueRanking.get(currentRank).Label);
                        system.debug(mapQueueIdToUserIDs2.containsKey(mapQueueNameToId.get(mapQueueRanking.get(currentRank).Label)));
                        system.debug( !mapQueueIdToUserIDs2.containsKey(mapQueueNameToId.get(mapQueueRanking.get(currentRank).Label))  && highestRank >= currentRank);
                        
                        while (highestRank >= currentRank && !mapQueueIdToUserIDs2.containsKey(mapQueueNameToId.get(mapQueueRanking.get(currentRank).Label)) ){
                            currentRank++;
                        }
                        system.debug('currentRank' + currentRank);
                        if(currentRank <= highestRank){
                            userPresentInQueuesAhead = true;
                            Integer counter = rankedLeads.size() > mapQueueIdToUserIDs2.get(mapQueueNameToId.get(mapQueueRanking.get(currentRank).Label)).size() ? mapQueueIdToUserIDs2.get(mapQueueNameToId.get(mapQueueRanking.get(currentRank).Label)).size() : rankedLeads.size(); 
                            system.debug('counter before' + counter);
                            List<Id> userIDs = new List<Id>(mapQueueIdToUserIDs2.get(mapQueueNameToId.get(mapQueueRanking.get(currentRank).Label)));
                            
                            List<String> userNames = new List<String>();
                            for(Id userID : userIDs){
                                if(mapUsers.containsKey(userID) ) {
                                    if(mapUsers.get(userID) != null){
                                        userNames.add(mapUsers.get(userID).Name);
                                    }
                                }
                            }
                            userNames.sort();
                            system.debug(mapQueueRanking.get(currentRank).Label);
                            system.debug(usernames.size());
                            Integer randomIndex = (Math.random() *(userNames.size())).intValue();     
                            List<String> sortedUserNames = new List<String>();
                            for(integer i = randomIndex ; i < userNames.size(); i ++){
                                sortedUserNames.add(userNames[i]);
                            }
                            for(integer i = 0 ; i < randomIndex; i ++){
                                sortedUserNames.add(userNames[i]);
                            }
                            system.debug(sortedUserNames);
                            system.debug(sortedUserNames.size());
                            for(Integer i = 0; i < counter; i++){
                                if(rankedLeads.size() > sizeOfLeadsAssigned){
                                    system.debug('sizeOfLeadsAssigned' + sizeOfLeadsAssigned);
                                    system.debug(rankedLeads[sizeOfLeadsAssigned]);
                                    rankedLeads[sizeOfLeadsAssigned].Agent__c = mapNameToUserID.get(sortedUserNames[i]);
                                    rankedLeads[sizeOfLeadsAssigned].Status__c = 'Allocated';
                                    leadAllocationRecordList.add(rankedLeads[sizeOfLeadsAssigned]);
                                }
                                
                                sizeOfLeadsAssigned++;
                            }
                        }
                        // currentRank++;
                        if(highestRank < currentRank){
                            
                            currentRank = rank;
                        }
                        if(!userPresentInQueuesAhead){
                            break;
                        }
                    }
                }
            }
        }
        if(leadAllocationRecordList.size() > 0){
            system.debug(leadAllocationRecordList);
            update leadAllocationRecordList;
        }
        if(isSchedulerRunning != null){
            isSchedulerRunning.Is_Running__c = false;
            update isSchedulerRunning;
        }
    }
}