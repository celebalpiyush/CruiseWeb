@isTest
private class TestLeadAssignmentUtility {
    
    static testMethod void leadUnitTest1(){
        //User u = [Select Id, Name From User where isActive = true and Is_Available__c = true];
        
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles',
                          UserName=uniqueUserName,
                          Is_Available__c = true,isActive = true);
        insert u;
        
        system.runAs(u){
        Timezone_Information__c otime = new Timezone_Information__c();
        otime.Name = 'Tokyo';
        otime.Timezone__c = 'Asia/Tokyo';
        insert otime;
        
        Lead leadData = new Lead();        
        leadData.Company = 'test';
        leadData.LastName = 'testLastName';
        leadData.Phone = '123445678';
        leadData.Email = 'test@gmail.com'; 
        leadData.City = 'test City';
        insert leadData;
        
        Lead_Allocation__c leadA = new Lead_Allocation__c();
        leadA.Status__c = 'New';        
        leadA.Lead__c = leadData.Id;
        //leadA.Agent__c = u[1].id;
        insert leadA;
       
        LeadAssignmentUtility oUtil = new LeadAssignmentUtility();
        LeadAssignmentUtility.leadAssignment();
        }
        
    }
    static testMethod void leadUnitTest2(){
        
        Is_Scheduler_Running__c isRun = new Is_Scheduler_Running__c();
        isRun.Is_Running__c = true;
        isRun.Name = 'Lead Allocation Scheduler';
        insert isRun;
        
        LeadAssignmentUtility oUtil = new LeadAssignmentUtility();
        LeadAssignmentUtility.leadAssignment();
        
    }
    
}