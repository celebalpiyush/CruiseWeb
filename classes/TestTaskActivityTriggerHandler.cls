@isTest
private class TestTaskActivityTriggerHandler{
    static testMethod void tasktestmethod() {
        
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles',
                          UserName=uniqueUserName);
        insert u;
        
        Lead leadData = new Lead(Company = 'Test Lead',LastName = 'Lead Last Name');
        insert leadData;
        
        Task t = new Task();
        t.Subject='Day 1 - Contact #1: Call & Email';
        t.Status='Completed';
        t.Priority='Normal';
        t.WhoId = leadData.id;
        insert t;    
    }
}