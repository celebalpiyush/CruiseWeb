public class OpportunityTriggerHandler {
	public static void beforeRecordInsert(List<Opportunity> newList) {
        removeNullString(newList);
    }
    
    public static void removeNullString(List<Opportunity> newlist) {
        String regex = '(?i)null';
        String replacedMessage;
        Pattern regexPattern = Pattern.compile(regex);
        
        for(Opportunity oppRecord : newlist ) {
            Matcher regexMatcher = regexPattern.matcher(oppRecord.Name);
            
            if(regexMatcher.find()) {
                oppRecord.Name = oppRecord.Name.replaceAll(regex, '').trim();
            }
        }
    }
}