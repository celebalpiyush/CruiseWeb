<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <formFactors>Large</formFactors>
    <navType>Console</navType>
    <tab>standard-Case</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Account</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-home</tab>
    <tab>Lead_Allocation__c</tab>
    <tab>Timezone_Information__c</tab>
    <tab>Agent_Availability_Matrix__c</tab>
    <uiType>Lightning</uiType>
    <utilityBar>LightningService_UtilityBar</utilityBar>
</CustomApplication>
