<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <fields>
        <fullName>Calls__c</fullName>
        <description>Number of calls received to this campaign number</description>
        <externalId>false</externalId>
        <label># Calls</label>
        <precision>6</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Click_Rate__c</fullName>
        <description>auto-calculated: # of clicks in the e-mail divided by # of e-mails delivered</description>
        <externalId>false</externalId>
        <formula>Clicks__c / Emails_Delivered__c</formula>
        <label>Click %</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Clicks__c</fullName>
        <description>total number of unique clicks on links inside the e-mail</description>
        <externalId>false</externalId>
        <inlineHelpText>total number of unique clicks on links inside the e-mail</inlineHelpText>
        <label># Clicks</label>
        <precision>6</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Conversion_percent__c</fullName>
        <description># of clicks / # of opens for the e-mail blast</description>
        <externalId>false</externalId>
        <formula>Clicks__c / E_mail_Opens__c</formula>
        <inlineHelpText>automatically calculates # Clicks / # Opens</inlineHelpText>
        <label>Conversion %</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>E_mail_Opens__c</fullName>
        <description>total number of e-mails that were opened</description>
        <externalId>false</externalId>
        <inlineHelpText>total number of e-mails that were opened</inlineHelpText>
        <label># E-mail Opens</label>
        <precision>6</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Emails_Delivered__c</fullName>
        <description>Total number of delivered e-mails from the e-mail blast</description>
        <externalId>false</externalId>
        <inlineHelpText>total messages sent - total messages bounced</inlineHelpText>
        <label>E-mails Delivered</label>
        <precision>6</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Landing_Page_Clicks__c</fullName>
        <description>total number of clicks on the landing page link in the e-mail</description>
        <externalId>false</externalId>
        <inlineHelpText>total number of clicks on the landing page link in the e-mail (doesn&apos;t include social media links etc). This only references clicks on the link to request a quote</inlineHelpText>
        <label>Landing Page Clicks</label>
        <precision>5</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Open_Percent__c</fullName>
        <description>auto-calculation: # e-mails opened divided by # of e-mails delivered</description>
        <externalId>false</externalId>
        <formula>E_mail_Opens__c / Emails_Delivered__c</formula>
        <label>Open %</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Start_Month__c</fullName>
        <externalId>false</externalId>
        <formula>month( StartDate )</formula>
        <label>Start Month</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <listViews>
        <fullName>AllActiveCampaigns</fullName>
        <columns>CAMPAIGN.NAME</columns>
        <columns>CAMPAIGN.START_DATE</columns>
        <columns>CAMPAIGN.END_DATE</columns>
        <columns>CAMPAIGN.CAMPAIGN_TYPE</columns>
        <columns>CAMPAIGN.STATUS</columns>
        <columns>CAMPAIGN.NUM_LEADS</columns>
        <columns>CAMPAIGN.NUM_OPPORTUNITIES</columns>
        <columns>CAMPAIGN.NUM_WON_OPPORTUNITIES</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>CAMPAIGN.ACTIVE</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <label>All Active Campaigns</label>
    </listViews>
    <listViews>
        <fullName>Daily_Queue_Sweep_Campaign</fullName>
        <columns>CAMPAIGN.NAME</columns>
        <columns>CAMPAIGN.START_DATE</columns>
        <columns>CAMPAIGN.END_DATE</columns>
        <columns>CAMPAIGN.BUDGETED_COST</columns>
        <columns>CAMPAIGN.ACTUAL_COST</columns>
        <columns>CAMPAIGN.CAMPAIGN_TYPE</columns>
        <columns>CAMPAIGN.STATUS</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>CAMPAIGN.NAME</field>
            <operation>contains</operation>
            <value>Sweep</value>
        </filters>
        <label>Daily Queue Sweep Campaign</label>
    </listViews>
    <listViews>
        <fullName>MyActiveCampaigns</fullName>
        <columns>CAMPAIGN.NAME</columns>
        <columns>CAMPAIGN.START_DATE</columns>
        <columns>CAMPAIGN.END_DATE</columns>
        <columns>CAMPAIGN.CAMPAIGN_TYPE</columns>
        <columns>CAMPAIGN.STATUS</columns>
        <filterScope>Mine</filterScope>
        <filters>
            <field>CAMPAIGN.ACTIVE</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <label>My Active Campaigns</label>
    </listViews>
    <webLinks>
        <fullName>ViewAllCampaignMembers</fullName>
        <availability>online</availability>
        <displayType>link</displayType>
        <encodingKey>UTF-8</encodingKey>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>500</height>
        <isResizable>true</isResizable>
        <linkType>url</linkType>
        <masterLabel>View All Campaign Members</masterLabel>
        <openType>newWindow</openType>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
        <url>/camp/rptcmpgncalldown.jsp?scope=1&amp;scopeid={!Campaign_Name}</url>
        <width>500</width>
    </webLinks>
    <webLinks>
        <fullName>ViewCampaignInfluenceReport</fullName>
        <availability>online</availability>
        <displayType>link</displayType>
        <encodingKey>UTF-8</encodingKey>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>500</height>
        <isResizable>true</isResizable>
        <linkType>url</linkType>
        <masterLabel>View Campaign Influence Report</masterLabel>
        <openType>newWindow</openType>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
        <url>/00O?rt=117&amp;break0=CAN&amp;scope=1&amp;scopeid_lkid={!Campaign.Id}&amp;scopeid={!Campaign_Name}&amp;c=CAN&amp;c=MR&amp;c=PCS&amp;c=CFN&amp;c=RN&amp;c=ON&amp;c=OA&amp;c=OS&amp;c=OCD&amp;c=AN&amp;s=OA&amp;duel0=CAN%2CMR%2CCFN%2CRN%2CAN%2CON%2CPCS%2COS%2COCD%2COA&amp;details=yes&amp;format=t</url>
        <width>500</width>
    </webLinks>
</CustomObject>
