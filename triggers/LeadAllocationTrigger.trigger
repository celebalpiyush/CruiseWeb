trigger LeadAllocationTrigger on Lead_Allocation__c (after update) {

    if(trigger.isAfter && trigger.isUpdate){
        LeadAllocationTriggerHandler.afterUpdate(trigger.new, trigger.oldMap);        
    }
}