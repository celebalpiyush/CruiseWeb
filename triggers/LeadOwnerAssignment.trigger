trigger LeadOwnerAssignment on Lead (before insert, after insert) {
    
    if(trigger.isInsert && trigger.isBefore){
        //-- STEP 1: list to hold the email of the lead record 
    List<String> leadEamilList = new List<String>();    
    List<String> leadPhoneList = new List<String>();
    Map<Id, Boolean> ownerAssignedMap = new Map<Id, Boolean>();
    
    List<LeadOwnerAssignmentInvalidPhoneList__c> LeadOwnerAssignmentInvalidPhoneObjectList = new List<LeadOwnerAssignmentInvalidPhoneList__c>();
    
    Map<String,String> invalidPhoneMap = new Map<String,String>();    
    LeadOwnerAssignmentInvalidPhoneObjectList = LeadOwnerAssignmentInvalidPhoneList__c.getall().values();        
    for(LeadOwnerAssignmentInvalidPhoneList__c eachObject : LeadOwnerAssignmentInvalidPhoneObjectList){
        invalidPhoneMap.put(eachObject.PhoneNumber__c,eachObject.PhoneNumber__c);
    }
    
    //-- STEP 2: loop the records and form the lead Id list
    for(Lead eachLead : Trigger.NEW){ 
        
        if(eachLead.LeadSource=='Website Lead' || eachLead.LeadSource=='E-mail blast'){
            
            if(eachLead.Email != null && String.isNotEmpty(eachLead.Email) && String.isNotBlank(eachLead.Email)){                 
                leadEamilList.add(eachLead.Email);                
            }   
            
            if(eachLead.Phone != null && String.isNotEmpty(eachLead.Phone) && String.isNotBlank(eachLead.Phone) && !invalidPhoneMap.containsKey(eachLead.Phone)){
                leadPhoneList.add(eachLead.Phone);
            }
            
        } 
    }

    
    //-- STEP 3: Perform SOQL query and build map to hold matching Email address as key and Contact Obj as value
    if(leadEamilList.size()>0 || leadPhoneList.size()>0){
        
        String query='';
        if(leadEamilList.size()>0 && leadPhoneList.size()>0){
            query='Select c.Phone,c.Fax,c.PersonEmail,c.Name,c.Owner.Name,c.OwnerId From Account c WHERE IsPersonAccount=true AND (PersonEmail IN :leadEamilList OR Phone IN :leadPhoneList OR Fax IN :leadPhoneList) AND c.Owner.IsActive=true AND c.Owner.LeadAssignmentFlowDisabled__c=false ORDER BY LastModifiedDate DESC';
        }else if(leadEamilList.size()>0){
            query='Select c.Phone,c.Fax,c.PersonEmail,c.Name,c.Owner.Name,c.OwnerId From Account c WHERE IsPersonAccount=true AND PersonEmail IN :leadEamilList AND c.Owner.IsActive=true AND c.Owner.LeadAssignmentFlowDisabled__c=false ORDER BY LastModifiedDate DESC';
        }else if(leadPhoneList.size()>0){
            query='Select c.Phone,c.Fax,c.PersonEmail,c.Name,c.Owner.Name,c.OwnerId From Account c WHERE IsPersonAccount=true AND (Phone IN :leadPhoneList OR Fax IN :leadPhoneList) AND c.Owner.IsActive=true AND c.Owner.LeadAssignmentFlowDisabled__c=false ORDER BY LastModifiedDate DESC';
        }
        
        if(query.trim().length()>0){
            
            //-- map to hold the data for final processing
            Map<String,Account> emailAndOwnerIdMap = new Map<String,Account>();
            Map<String,Account> primaryPhoneAndOwnerIdMap = new Map<String,Account>(); 
            Map<String,Account> secondaryPhoneAndOwnerIdMap = new Map<String,Account>();  
            
            //-- SOQL For loop query
            for (Account accountRec : Database.query(query)) {
                
                //-- add data to map if it does not contains already
                if(!emailAndOwnerIdMap.containsKey(accountRec.PersonEmail)){
                    emailAndOwnerIdMap.put(accountRec.PersonEmail, accountRec);
                }                
                
                 //-- add data to map if it does not contains already
                if(accountRec.Phone!=null && accountRec.Phone!='' && !invalidPhoneMap.containsKey(accountRec.Phone) && !primaryPhoneAndOwnerIdMap.containsKey(accountRec.Phone)){
                    primaryPhoneAndOwnerIdMap.put(accountRec.Phone, accountRec);
                }
                
                //-- add data to map if it does not contains already
                if(accountRec.Fax!=null && accountRec.Fax!='' && !invalidPhoneMap.containsKey(accountRec.Fax) && !secondaryPhoneAndOwnerIdMap.containsKey(accountRec.Fax)){
                    secondaryPhoneAndOwnerIdMap.put(accountRec.Fax, accountRec);
                }                
            }
            
            //-- STEP 4: Perform final processing to assign Lead owner if macthing Personal Account owner is found
            if(emailAndOwnerIdMap.size()>0 || primaryPhoneAndOwnerIdMap.size()>0 || secondaryPhoneAndOwnerIdMap.size()>0){            
                for(Lead eachLead : Trigger.NEW){         
                    if((eachLead.LeadSource=='Website Lead' || eachLead.LeadSource=='E-mail blast')){             
                        
                        if(eachLead.Email != null && String.isNotEmpty(eachLead.Email) && String.isNotBlank(eachLead.Email) && emailAndOwnerIdMap.containsKey(eachLead.Email)){
                            eachLead.OwnerId=emailAndOwnerIdMap.get(eachLead.Email).OwnerId;
                            eachLead.OwnerAssigned__c=true;
                        }
                        else if(eachLead.Phone != null && String.isNotEmpty(eachLead.Phone) && String.isNotBlank(eachLead.Phone)){
                            
                            if(primaryPhoneAndOwnerIdMap.containsKey(eachLead.Phone)){
                                eachLead.OwnerId=primaryPhoneAndOwnerIdMap.get(eachLead.Phone).OwnerId;
                                eachLead.OwnerAssigned__c=true;
                            }else if(secondaryPhoneAndOwnerIdMap.containsKey(eachLead.Phone)){
                                eachLead.OwnerId=secondaryPhoneAndOwnerIdMap.get(eachLead.Phone).OwnerId;
                                eachLead.OwnerAssigned__c=true;
                            }                           
                        }
                    }    
                }            
            }
        }
    }
    } 
    if(trigger.isInsert && trigger.isAfter){
          // Populate Lead Allocation object if lead is not assigend to any agent
        List<Lead_Allocation__c> leadAllocations = new List<Lead_Allocation__c>();
        for(Lead eachLead : Trigger.New) {
            if(!eachLead.OwnerAssigned__c) {
                Lead_Allocation__c leadAllocation = new Lead_Allocation__c();
                leadAllocation.Lead__c = eachLead.Id;   
                leadAllocation.Lead_Created_Time__c = System.Now();
                leadAllocations.add(leadAllocation);
            }
        }
        
        System.debug(leadAllocations);
        
        if(leadAllocations.size() > 0) {
            insert leadAllocations;
        }  
    } 
}