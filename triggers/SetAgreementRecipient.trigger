trigger SetAgreementRecipient on echosign_dev1__SIGN_Agreement__c (before insert) {
	for( echosign_dev1__SIGN_Agreement__c agreement : Trigger.new ) {
		if( agreement.echosign_dev1__Opportunity__c == null ) {
			continue;
		}
		
		Opportunity opp = [SELECT Account.PersonContactId from Opportunity where Id = :agreement.echosign_dev1__Opportunity__c];
		
		agreement.echosign_dev1__Recipient__c = opp.Account.PersonContactId;
	}
}