trigger TaskActivityTrigger on Task (after insert, after update) {
    if(trigger.isAfter && trigger.isInsert){
        TaskActivityTriggerHandler.afterInsert(trigger.new);        
    }
    if(trigger.isAfter && trigger.isupdate){
        TaskActivityTriggerHandler.afterUpdate(trigger.new);        
    }
}