<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Client_Task_Check_Successful_Contact</fullName>
        <field>Successful_Contact__c</field>
        <literalValue>1</literalValue>
        <name>Client Task - Check Successful Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contact_Result_Auto_email</fullName>
        <field>Contact_Result__c</field>
        <literalValue>*Auto-Response Email Sent</literalValue>
        <name>Update Contact Result - Auto email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contact_Result_Google_Lead</fullName>
        <field>Contact_Result__c</field>
        <literalValue>*Google Lead Source Details</literalValue>
        <name>Update Contact Result - Google Lead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>2nd New - Client - Subsequent Followup Trigger 2</fullName>
        <actions>
            <name>Day_4_Contact_3_Still_Interested_email_No_For_Now</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Contact_Result__c</field>
            <operation>equals</operation>
            <value>Bad Email,Emailed,Busy,Bad number,Left message &amp; emailed,Left message,No answer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Day 2 - Contact #2: Call &amp; &quot;Are you&quot; email</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Client - Subsequent Followup Trigger 1</fullName>
        <actions>
            <name>Day_2_Contact_2_Call</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Contact_Result__c</field>
            <operation>equals</operation>
            <value>Bad Email,Emailed,Busy,Bad number,Left message &amp; emailed,Left message,No answer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Day 1 - Contact #1: Call &amp; Email</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Client - Subsequent Followup Trigger 2</fullName>
        <actions>
            <name>Day_4_Contact_3_Call_Help_me_Help_you_email</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.Contact_Result__c</field>
            <operation>equals</operation>
            <value>Bad Email,Emailed,Busy,Bad number,Left message &amp; emailed,Left message,No answer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Day 2 - Contact #2: Call &amp; &quot;Are you&quot; email</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Client - Subsequent Followup Trigger 3</fullName>
        <actions>
            <name>Day_7_Contact_4_Call_Are_you_email</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.Contact_Result__c</field>
            <operation>equals</operation>
            <value>Bad Email,Emailed,Busy,Bad number,Left message &amp; emailed,Left message,No answer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>contains</operation>
            <value>Day 4 - Contact #3: Call &amp; “Help me,Help you” email</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Client - Subsequent Followup Trigger 4</fullName>
        <actions>
            <name>Day_10_Contact_5_Call</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.Contact_Result__c</field>
            <operation>equals</operation>
            <value>Bad Email,Emailed,Busy,Bad number,Left message &amp; emailed,Left message,No answer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Day 7 - Contact #4: Call &amp; “Are you” email</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Client - Subsequent Followup Trigger 5</fullName>
        <actions>
            <name>Day_30_Contact_6_Thank_You_email</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Contact_Result__c</field>
            <operation>equals</operation>
            <value>Bad Email,Emailed,Busy,Bad number,Left message &amp; emailed,Left message,No answer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Day 7 - Contact #4: Last Call</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Client - Successful Contact</fullName>
        <actions>
            <name>Client_Task_Check_Successful_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Contact_Result__c</field>
            <operation>equals</operation>
            <value>Made contact,Emailed,Spoke - set follow up,Left msg – Truncated f/u</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Google Lead Source Details</fullName>
        <actions>
            <name>Update_Contact_Result_Google_Lead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Lead Source Details</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead Auto-Response</fullName>
        <actions>
            <name>Update_Contact_Result_Auto_email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Email: Cruise Web Quote Request Confirmation</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Left Message - Day 1 Followup</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Task.Contact_Result__c</field>
            <operation>equals</operation>
            <value>Left message</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NEW Client - Subsequent Followup Trigger 2</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Task.Contact_Result__c</field>
            <operation>equals</operation>
            <value>Bad Email,Emailed,Busy,Bad number,Left message &amp; emailed,Left message,No answer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Day 2 - Contact #2: Call &amp; &quot;Are you&quot; email</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New - Client - Subsequent Followup Trigger 2</fullName>
        <actions>
            <name>Day_3_Contact_3_Call_Help_me_Help_you_email</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.Contact_Result__c</field>
            <operation>equals</operation>
            <value>Bad Email,Emailed,Busy,Bad number,Left message &amp; emailed,Left message,No answer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Day 2 - Contact #2: Call &amp; &quot;Are you&quot; email</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New - Client - Subsequent Followup Trigger 3</fullName>
        <actions>
            <name>Day_7_Contact_4_Still_Interested_email</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Contact_Result__c</field>
            <operation>equals</operation>
            <value>Bad Email,Emailed,Busy,Bad number,Left message &amp; emailed,Left message,No answer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>contains</operation>
            <value>Day 3 - Contact #3: Call &amp; “Help me,Help you” email</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Subsequent 6 Month Reminder</fullName>
        <actions>
            <name>Past_Passenger_6_Month_Client_Call</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Past Passenger - 6 Month Client Call</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Day_10_Contact_5_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Day 10 - Contact #5: Call</subject>
    </tasks>
    <tasks>
        <fullName>Day_1_Send_Your_request_email</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Day 1 – Send “Your request” email</subject>
    </tasks>
    <tasks>
        <fullName>Day_2_2nd_Contact_Attempt</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Day 2 - 2nd Contact Attempt</subject>
    </tasks>
    <tasks>
        <fullName>Day_2_Contact_2_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Day 2 - Contact #2: Call &amp; &quot;Are you&quot; email</subject>
    </tasks>
    <tasks>
        <fullName>Day_2_Send_Are_you_email</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Day 2 – Send “Are you” email</subject>
    </tasks>
    <tasks>
        <fullName>Day_30_Contact_6_Thank_You_email</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>23</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Day 30 - Contact #6: “Thank You” email</subject>
    </tasks>
    <tasks>
        <fullName>Day_3_Contact_3_Call_Help_me_Help_you_email</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Day 3 - Contact #3: Call &amp; “Help me, Help you” email</subject>
    </tasks>
    <tasks>
        <fullName>Day_4_Contact_3_Call_Help_me_Help_you_email</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Day 4 - Contact #3: Call &amp; “Help me, Help you” email</subject>
    </tasks>
    <tasks>
        <fullName>Day_4_Contact_3_Still_Interested_email_No_For_Now</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Day 4 - Contact #3: “Still Interested” email + No For Now</subject>
    </tasks>
    <tasks>
        <fullName>Day_4_Send_Help_me_Help_you_email</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Day 4  - Send “Help me, Help you” email</subject>
    </tasks>
    <tasks>
        <fullName>Day_7_Contact_4_Call_Are_you_email</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Day 7 - Contact #4: Last Call</subject>
    </tasks>
    <tasks>
        <fullName>Day_7_Contact_4_Last_Email_Still_Interested</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Day 7 - Contact #4: Last Email Still Interested</subject>
    </tasks>
    <tasks>
        <fullName>Day_7_Contact_4_Still_Interested_email</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>4</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Day 7 - Contact #4: “Still Interested” email</subject>
    </tasks>
    <tasks>
        <fullName>Past_Passenger_6_Month_Client_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>180</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Past Passenger - 6 Month Client Call</subject>
    </tasks>
    <tasks>
        <fullName>X2nd_day_call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Task.last_modified_time__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>2nd day call</subject>
    </tasks>
</Workflow>
