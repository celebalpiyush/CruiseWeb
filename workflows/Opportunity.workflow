<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Assigned_Client_Stage</fullName>
        <field>StageName</field>
        <literalValue>Assigned Client</literalValue>
        <name>Assigned Client Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Calculate_Return_Home_Date</fullName>
        <field>Return_Home_Date__c</field>
        <formula>Sail_Date__c +  No_of_Nights__c</formula>
        <name>Calculate Return Home Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_stage_back_to_recommending</fullName>
        <field>StageName</field>
        <literalValue>Recommending</literalValue>
        <name>Change stage back to recommending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_stage_to_booked</fullName>
        <field>StageName</field>
        <literalValue>Booked!</literalValue>
        <name>Change stage to booked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_stage_to_qualifying</fullName>
        <field>StageName</field>
        <literalValue>Qualifying</literalValue>
        <name>Change stage to qualifying</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_stage_to_quoting</fullName>
        <field>StageName</field>
        <literalValue>Quoting</literalValue>
        <name>Change stage to quoting</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_stage_to_recommending</fullName>
        <field>StageName</field>
        <literalValue>Recommending</literalValue>
        <name>Change stage to recommending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Qualifying_Checkbox</fullName>
        <field>Qualifying__c</field>
        <literalValue>1</literalValue>
        <name>Check Qualifying Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Booked_Date</fullName>
        <field>Booked_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Booked Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_No_for_Now_Stage</fullName>
        <field>StageName</field>
        <literalValue>No For Now</literalValue>
        <name>Update No for Now Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Booked Stage</fullName>
        <actions>
            <name>Change_stage_to_booked</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Booked_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Insurance_CC_Auth_follow_up_if_necessary</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Insurance_call_email</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Booked__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.No_for_Now__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Booked Stage back to recommending</fullName>
        <actions>
            <name>Change_stage_back_to_recommending</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Booked__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Client - Initial Followup Method Activities</fullName>
        <actions>
            <name>Day_1_Contact_1_Call_Email</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <formula>ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Comment Characters</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Booked__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Followup after Most Recent Sail Date</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Booked!</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Sail_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Oppty</fullName>
        <actions>
            <name>Assigned_Client_Stage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>No fo Now Stage</fullName>
        <actions>
            <name>Update_No_for_Now_Stage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.No_for_Now__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.No_For_Now_Reason__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Qualifying Comments</fullName>
        <actions>
            <name>Check_Qualifying_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Qualifying_Comments__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Qualifying Stage</fullName>
        <actions>
            <name>Change_stage_to_qualifying</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Qualifying__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.No_for_Now__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Quoting Stage</fullName>
        <actions>
            <name>Change_stage_to_quoting</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Quoting__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.No_for_Now__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Quoting Stage - back to recommending</fullName>
        <actions>
            <name>Change_stage_back_to_recommending</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(ISPICKVAL( StageName , &quot;Quoting&quot;),
ISCHANGED( Quoting__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Recommending Stage</fullName>
        <actions>
            <name>Change_stage_to_recommending</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Recommending__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Quoting__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.No_for_Now__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Return Home Date - Calculation</fullName>
        <actions>
            <name>Calculate_Return_Home_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Welcome_Home_Call_Reminder</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Sail_Date__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Sail Date Updated</fullName>
        <actions>
            <name>Bon_Voyage_Call_Reminder</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Sail_Date__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Bon_Voyage_Call_Reminder</fullName>
        <assignedToType>owner</assignedToType>
        <description>Don&apos;t forget the bon voyage call for this client.</description>
        <dueDateOffset>-7</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.Sail_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>&quot;Bon Voyage Call&quot; - Reminder</subject>
    </tasks>
    <tasks>
        <fullName>Day_1011_5th_Contact_Attempt</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>9</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Day 10/11 - 5th Contact Attempt</subject>
    </tasks>
    <tasks>
        <fullName>Day_1_Contact_1_Call_Email</fullName>
        <assignedToType>owner</assignedToType>
        <description>If you don&apos;t get through this time, try calling a second time today (if you have time) to increase the odds of contacting and closing this client.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Day 1 - Contact #1: Call &amp; Email</subject>
    </tasks>
    <tasks>
        <fullName>Day_2_2nd_Contact_Attempt</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Day 2 - 2nd Contact Attempt</subject>
    </tasks>
    <tasks>
        <fullName>Day_30_Contact_6_Thank_You_email</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>29</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Day 30 - Contact #6: “Thank You” email</subject>
    </tasks>
    <tasks>
        <fullName>Day_3_3rd_Contact_Attempt</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Day 3 - 3rd Contact Attempt</subject>
    </tasks>
    <tasks>
        <fullName>Day_4_3rd_Contact_Attempt</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Day 4 - 3rd Contact Attempt</subject>
    </tasks>
    <tasks>
        <fullName>Day_7_4th_Contact_Attempt</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>6</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Day 7 - 4th Contact Attempt</subject>
    </tasks>
    <tasks>
        <fullName>Insurance_CC_Auth_follow_up_if_necessary</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.Booked_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Insurance &amp; CC Auth follow up (if necessary)</subject>
    </tasks>
    <tasks>
        <fullName>Insurance_call_email</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>8</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.Booked_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Insurance: call/email (if necessary)</subject>
    </tasks>
    <tasks>
        <fullName>Welcome_Home_Call_Reminder</fullName>
        <assignedToType>owner</assignedToType>
        <description>Don&apos;t forget to welcome this client home from their cruise.</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.Return_Home_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>&quot;Welcome Home Call&quot; - Reminder</subject>
    </tasks>
</Workflow>
