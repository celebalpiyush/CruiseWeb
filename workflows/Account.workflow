<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Return_client_to_manager_email_alert</fullName>
        <ccEmails>michaelh@cruiseweb.com</ccEmails>
        <description>Return client to manager email alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>betht@cruiseweb.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>clairea@cruiseweb.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Client_Returned_to_Management</template>
    </alerts>
    <rules>
        <fullName>Client Followup after Most Recent Sail Date</fullName>
        <actions>
            <name>Past_Passenger_3_Month_Client_Call</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Past_Passenger_6_Month_Client_Call</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Most_Recent_Sail_Date__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Client Returned to Management</fullName>
        <actions>
            <name>Return_client_to_manager_email_alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Return_to_Manager__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Past_Passenger_12_Month_Client_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>365</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Account.Most_Recent_Sail_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Past Passenger - 12 Month Client Call</subject>
    </tasks>
    <tasks>
        <fullName>Past_Passenger_3_Month_Client_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>90</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Account.Most_Recent_Sail_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Past Passenger - 3 Month Client Call</subject>
    </tasks>
    <tasks>
        <fullName>Past_Passenger_6_Month_Client_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>180</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Account.Most_Recent_Sail_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Past Passenger - 6 Month Client Call</subject>
    </tasks>
</Workflow>
